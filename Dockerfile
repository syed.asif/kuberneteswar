FROM tomcat:9
WORKDIR /tmp/
RUN update-ca-certificates 
RUN wget https://tomcat.apache.org/tomcat-8.5-doc/appdev/sample/sample.war
RUN cp /tmp/sample.war /usr/local/tomcat/webapps/
EXPOSE 8080
